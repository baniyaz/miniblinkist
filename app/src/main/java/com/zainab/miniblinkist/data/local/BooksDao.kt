package com.zainab.miniblinkist.data.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable

@Dao
interface BooksDao {
    @Query("DELETE FROM book")
    fun deleteAll()

    @Query("SELECT * FROM book")
    fun getBooks(): Flowable<List<BookEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveBook(bookEntity: BookEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(movies: List<BookEntity>)
}