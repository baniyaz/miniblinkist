package com.zainab.miniblinkist.data

import com.zainab.miniblinkist.data.local.BookEntity
import com.zainab.miniblinkist.data.remote.BookService
import com.zainab.miniblinkist.domain.ISchedulerFactory
import com.zainab.miniblinkist.domain.model.Book
import com.zainab.miniblinkist.domain.repository.IBookRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import javax.inject.Inject

class BooksRepository @Inject constructor(private val bookService: BookService,
                                          private val schedulerFactory: ISchedulerFactory,
                                          private val bookStore: ReactiveStore<BookEntity>,
                                          private val mapper: Mapper
): IBookRepository {
    override fun fetchRemoteBooks(): Completable {
        return bookService.getBooks()
                .subscribeOn(schedulerFactory.io())
                .observeOn(schedulerFactory.computation())
                .flatMapObservable { Observable.fromIterable(it) }
                .map { mapper.toBookEntity(it) }
                .toList()
                .doOnSuccess { bookStore.replaceAll(it) }
                .toCompletable()
    }

    override fun getLocalBooks(): Flowable<List<Book>> {
        return bookStore.getAll()
                .map { mapper.toBookList(it) }
    }
}