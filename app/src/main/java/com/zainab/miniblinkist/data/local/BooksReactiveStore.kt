package com.zainab.miniblinkist.data.local

import com.zainab.miniblinkist.data.ReactiveStore
import io.reactivex.Flowable
import java.util.*
import javax.inject.Inject

class BooksReactiveStore @Inject constructor(private val booksDao: BooksDao): ReactiveStore<BookEntity> {
    override fun storeSingular(t: BookEntity) {
        booksDao.saveBook(t)
    }

    override fun storeAll(objects: List<BookEntity>) {
        booksDao.insertAll(objects)
    }

    override fun replaceAll(objects: List<BookEntity>) {
        booksDao.deleteAll()
        storeAll(objects)
    }

    override fun getAll(): Flowable<List<BookEntity>> {
        return booksDao.getBooks()
                .map { books ->
                    if (books.isEmpty()) Collections.emptyList()
                    else books
                }
    }
}