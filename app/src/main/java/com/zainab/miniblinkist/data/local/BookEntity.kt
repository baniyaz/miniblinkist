package com.zainab.miniblinkist.data.local

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "book")
data class BookEntity(
        @PrimaryKey
        val id: String,
        val title: String,
        val author: String,
        val description: String,
        val publishedDate: String,
        val thumbnail: String,
        val premium: Boolean
)