package com.zainab.miniblinkist.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = [(BookEntity::class)], version = 1)
abstract class BooksDatabase : RoomDatabase() {
    abstract fun booksDao(): BooksDao
}