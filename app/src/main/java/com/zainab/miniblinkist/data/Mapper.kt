package com.zainab.miniblinkist.data

import com.zainab.miniblinkist.data.local.BookEntity
import com.zainab.miniblinkist.domain.model.Book
import javax.inject.Inject

class Mapper @Inject constructor(){
    fun toBookList(entities: List<BookEntity>): List<Book> {
        val res = mutableListOf<Book>()
        entities.forEach {
            res.add(Book(it.id, it.title, it.author, it.description, it.publishedDate, it.thumbnail, it.premium))
        }
        return res
    }

    fun toBookEntity(book: Book) = BookEntity(book.id, book.title, book.author, book.description, book.publishedDate, book.thumbnail, book.premium)
}