package com.zainab.miniblinkist.data.remote

import com.zainab.miniblinkist.domain.model.Book
import io.reactivex.Single
import retrofit2.http.GET

interface BookService {
    @GET("/books")
    fun getBooks(): Single<List<Book>>

    companion object {
        const val API_URL = "http://api.miniblinkist.com/"
    }
}