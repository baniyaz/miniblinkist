package com.zainab.miniblinkist.data.remote

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.zainab.miniblinkist.BuildConfig
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Factory responsible for creating [BookService] and it's components
 */
object BookServiceFactory {
    fun buildBooksService(): BookService {
        return bookService(gson(), client(httpLogginginterceptor(BuildConfig.DEBUG)), BookService.API_URL)
    }

    private fun bookService(gson: Gson, client: OkHttpClient, url: String): BookService {
        return Retrofit.Builder()
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(url)
                .build()
                .create(BookService::class.java)
    }

    private fun client(httpLoggingInterceptor: HttpLoggingInterceptor) = OkHttpClient.Builder()
            .connectTimeout(2, TimeUnit.MINUTES)
            .writeTimeout(2, TimeUnit.MINUTES)
            .readTimeout(2, TimeUnit.MINUTES)
            .addInterceptor(httpLoggingInterceptor)
            .addInterceptor(mockResponseInterceptor())
            .build()

    private fun mockResponseInterceptor() = Interceptor { chain ->
        val inputStream = this.javaClass.classLoader.getResourceAsStream("res/raw/books.json")

        val inputString = inputStream.bufferedReader().use { it.readText() }

        Response.Builder()
                .body(ResponseBody.create(MediaType.parse("application/json"), inputString))
                .request(chain.request())
                .protocol(Protocol.HTTP_2)
                .message("Mock API response read from res/raw/books.json")
                .code(200)
                .build()
    }

    private fun gson() = GsonBuilder()
            .setLenient()
            .setDateFormat("yyyy-MM-dd")
            .create()

    private fun httpLogginginterceptor(isDebug: Boolean) = HttpLoggingInterceptor().apply {
        level = if (isDebug)
            HttpLoggingInterceptor.Level.BODY
        else
            HttpLoggingInterceptor.Level.NONE
    }
}