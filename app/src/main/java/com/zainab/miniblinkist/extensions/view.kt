package com.zainab.miniblinkist.extensions

import android.view.View
import android.widget.ImageView
import com.squareup.picasso.Picasso

fun ImageView.fromUrl(url: String) {
    Picasso.with(context)
            .load(url)
            .into(this)
}

fun View.show() {
   visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}