package com.zainab.miniblinkist.domain.usecase.base

import io.reactivex.subscribers.DisposableSubscriber

class UsecaseObserver {
    open class RxFlowable<T>: DisposableSubscriber<T>() {
        override fun onError(t: Throwable?) {}
        override fun onComplete() {}
        override fun onNext(t: T) {}
    }
}