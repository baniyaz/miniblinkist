package com.zainab.miniblinkist.domain.usecase.base

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * This is a base request to be implemented by all requests. It contains how requests are executed and disposed.
 * It returns a [Observable].
 */
abstract class UseCase{
    private val disposables = CompositeDisposable()

    /**
     * Dispose from current [CompositeDisposable].
     */
    fun dispose() {
        if (!disposables.isDisposed) disposables.dispose()
    }

    /**
     * Dispose from current [CompositeDisposable].
     */
    fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }
}
