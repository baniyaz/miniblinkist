package com.zainab.miniblinkist.domain.usecase.base

import com.zainab.miniblinkist.domain.ISchedulerFactory
import io.reactivex.Completable

abstract class CompletableUseCase<in Param> constructor(private val schedulerFactory: ISchedulerFactory)
    : UseCase(){

    abstract fun build(param: Param?): Completable

    /**
     * Executes the current use case.
     */
    open fun execute(onComplete: () -> Unit, onError: (Throwable) -> Unit, param: Param?) {
        val completable = this.build(param)
                .subscribeOn(schedulerFactory.io())
                .observeOn(schedulerFactory.main()) as Completable
        addDisposable(completable.subscribe(onComplete, onError))
    }
}
