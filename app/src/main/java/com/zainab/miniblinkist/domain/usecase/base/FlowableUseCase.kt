package com.zainab.miniblinkist.domain.usecase.base

import com.zainab.miniblinkist.domain.ISchedulerFactory
import io.reactivex.Flowable

abstract class FlowableUseCase<T, in Param> constructor(private val schedulerFactory: ISchedulerFactory)
    : UseCase() {

    abstract fun build(param: Param?): Flowable<T>

    /**
     * Executes the current use case.
     */
    open fun execute(observer: UsecaseObserver.RxFlowable<T>, param: Param?) {
        val flowable = this.build(param)
                .subscribeOn(schedulerFactory.io())
                .observeOn(schedulerFactory.main()) as Flowable<T>
        addDisposable(flowable.subscribeWith(observer))
    }
}
