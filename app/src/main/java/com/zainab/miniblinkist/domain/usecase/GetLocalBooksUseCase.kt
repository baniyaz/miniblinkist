package com.zainab.miniblinkist.domain.usecase

import com.zainab.miniblinkist.domain.ISchedulerFactory
import com.zainab.miniblinkist.domain.model.Book
import com.zainab.miniblinkist.domain.repository.IBookRepository
import com.zainab.miniblinkist.domain.usecase.base.FlowableUseCase
import javax.inject.Inject

class GetLocalBooksUseCase @Inject constructor(val bookRepository: IBookRepository, schedulerFactory: ISchedulerFactory)
    : FlowableUseCase<List<Book>, Void>(schedulerFactory) {

    override fun build(param: Void?) = bookRepository.getLocalBooks()
}
