package com.zainab.miniblinkist.domain.repository

import com.zainab.miniblinkist.domain.model.Book
import io.reactivex.Completable
import io.reactivex.Flowable

interface IBookRepository {
    fun fetchRemoteBooks(): Completable
    fun getLocalBooks(): Flowable<List<Book>>
}