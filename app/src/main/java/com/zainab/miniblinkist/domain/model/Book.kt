package com.zainab.miniblinkist.domain.model

data class Book (
        val id: String,
        val title: String,
        val author: String,
        val description: String,
        val publishedDate: String,
        val thumbnail: String,
        val premium: Boolean
)