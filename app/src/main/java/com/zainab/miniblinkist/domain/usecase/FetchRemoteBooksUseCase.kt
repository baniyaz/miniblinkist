package com.zainab.miniblinkist.domain.usecase

import com.zainab.miniblinkist.domain.ISchedulerFactory
import com.zainab.miniblinkist.domain.repository.IBookRepository
import com.zainab.miniblinkist.domain.usecase.base.CompletableUseCase
import io.reactivex.Completable
import javax.inject.Inject

class FetchRemoteBooksUseCase @Inject constructor(private val bookRepository: IBookRepository,
                                                  private val schedulersFactory: ISchedulerFactory):
        CompletableUseCase<Void>(schedulersFactory ) {

    override fun build(param: Void?): Completable = bookRepository.fetchRemoteBooks().subscribeOn(schedulersFactory.io())
}