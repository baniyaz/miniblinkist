package com.zainab.miniblinkist

import android.app.Activity
import android.app.Application
import com.zainab.miniblinkist.presentation.dependencyinjection.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class App: Application(), HasActivityInjector {

    override fun activityInjector(): AndroidInjector<Activity> = activityDispatchingInjector

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent
                .builder()
                .application(this)
                .build()
                .inject(this)
    }

    @Inject
    lateinit var activityDispatchingInjector: DispatchingAndroidInjector<Activity>
}