package com.zainab.miniblinkist.presentation.dependencyinjection

import android.app.Application
import com.zainab.miniblinkist.App
import com.zainab.miniblinkist.presentation.dependencyinjection.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        DatabaseModule::class,
        ViewModelModule::class,
        ActivityModule::class,
        ApiModule::class
])
@Singleton
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    fun inject(application: App)
}