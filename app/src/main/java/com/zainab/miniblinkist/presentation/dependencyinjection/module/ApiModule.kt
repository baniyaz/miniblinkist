package com.zainab.miniblinkist.presentation.dependencyinjection.module

import com.zainab.miniblinkist.data.BooksRepository
import com.zainab.miniblinkist.data.remote.BookServiceFactory
import com.zainab.miniblinkist.domain.ISchedulerFactory
import com.zainab.miniblinkist.domain.repository.IBookRepository
import com.zainab.miniblinkist.presentation.SchedulersFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApiModule {
    @Provides
    @Singleton
    fun providesBookService() = BookServiceFactory.buildBooksService()

    @Provides
    @Singleton
    fun providesSchedulersFactory(): ISchedulerFactory = SchedulersFactory()

    @Provides
    @Singleton
    fun providesBookRepository(booksRepository: BooksRepository): IBookRepository = booksRepository
}