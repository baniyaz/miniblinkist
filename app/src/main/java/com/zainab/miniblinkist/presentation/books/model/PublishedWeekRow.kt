package com.zainab.miniblinkist.presentation.books.model


class PublishedWeekRow(val publishedWeek: String): BookRow("", "", "", "", "", "", false) {
    override val type = BookRow.PUBLISHED_WEEK
}