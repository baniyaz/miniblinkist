package com.zainab.miniblinkist.presentation.books.model

open class BookRow(
        open val id: String,
        open val title: String,
        open val author: String,
        open val description: String,
        open val publishedDate: String,
        open val thumbnail: String,
        open  val premium: Boolean) {

    companion object {
        const val PUBLISHED_WEEK = 0
        const val DISPLAY = 1
    }

    open val type: Int = -1
}