package com.zainab.miniblinkist.presentation.dependencyinjection.module

import android.arch.lifecycle.ViewModelProvider
import com.zainab.miniblinkist.presentation.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
internal abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}