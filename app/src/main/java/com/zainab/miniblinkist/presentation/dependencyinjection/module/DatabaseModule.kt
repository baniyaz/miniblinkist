package com.zainab.miniblinkist.presentation.dependencyinjection.module

import android.arch.persistence.room.Room
import android.content.Context
import com.zainab.miniblinkist.data.ReactiveStore
import com.zainab.miniblinkist.data.local.BookEntity
import com.zainab.miniblinkist.data.local.BooksDao
import com.zainab.miniblinkist.data.local.BooksDatabase
import com.zainab.miniblinkist.data.local.BooksReactiveStore
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {
    @Provides
    @Singleton
    fun providesBooksDatabase(context: Context): BooksDatabase {
        return Room.databaseBuilder(context, BooksDatabase::class.java, "books-database").build()
    }

    @Provides
    @Singleton
    fun providesMoviesDao(database: BooksDatabase): BooksDao = database.booksDao()

    @Provides
    @Singleton
    fun providesMoviesReactiveStore(store: BooksReactiveStore): ReactiveStore<BookEntity> = store
}