package com.zainab.miniblinkist.presentation.dependencyinjection.module


import com.zainab.miniblinkist.presentation.books.BooksActivity
import com.zainab.miniblinkist.presentation.books.BooksModule
import com.zainab.miniblinkist.presentation.dependencyinjection.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
@ActivityScope
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = [BooksModule::class])
    @ActivityScope
    internal abstract fun bindMainActivity(): BooksActivity
}
