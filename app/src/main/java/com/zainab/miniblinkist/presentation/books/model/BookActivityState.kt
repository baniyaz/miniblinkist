package com.zainab.miniblinkist.presentation.books.model


class BookActivityState(val books:List<BookRow>? = listOf(), val message: String = "", val errored: Boolean = false)