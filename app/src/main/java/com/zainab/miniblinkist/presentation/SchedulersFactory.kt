package com.zainab.miniblinkist.presentation

import com.zainab.miniblinkist.domain.ISchedulerFactory
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SchedulersFactory: ISchedulerFactory {
    override fun main(): Scheduler = AndroidSchedulers.mainThread()

    override fun io(): Scheduler = Schedulers.io()

    override fun computation(): Scheduler= Schedulers.computation()

    override fun trampoline(): Scheduler = Schedulers.trampoline()
}