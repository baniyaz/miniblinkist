package com.zainab.miniblinkist.presentation.books

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.zainab.miniblinkist.domain.model.Book
import com.zainab.miniblinkist.domain.usecase.FetchRemoteBooksUseCase
import com.zainab.miniblinkist.domain.usecase.GetLocalBooksUseCase
import com.zainab.miniblinkist.domain.usecase.base.UsecaseObserver
import com.zainab.miniblinkist.presentation.books.model.BookActivityState
import com.zainab.miniblinkist.presentation.books.model.BookRow
import com.zainab.miniblinkist.presentation.books.model.Mapper
import javax.inject.Inject

class BooksActivityViewModel @Inject constructor(
        private val getLocalBooksUseCase: GetLocalBooksUseCase,
        private val fetchRemoteBooksUseCase: FetchRemoteBooksUseCase,
        private val mapper: Mapper): ViewModel() {

    private val liveData = MutableLiveData<BookActivityState>()

    private var books = listOf<Book>()

    private val TAG = "BooksViewModel"

    init {
        bindToStream()
        refreshData()
    }

    fun getBooksLiveData(): LiveData<BookActivityState> = liveData

    private fun refreshData()  {
        fetchRemoteBooksUseCase.execute({}, { error -> postError(error)}, null)
    }

    private fun bindToStream(){
        getLocalBooksUseCase.execute(GetBooksObserver(), null)
    }

    fun sortAscending() {
        postBooks(mapper.toRowListAsc(books))
    }

    fun sortDescending() {
        postBooks(mapper.toRowListDesc(books))
    }

    private inner class GetBooksObserver : UsecaseObserver.RxFlowable<List<Book>>() {
        override fun onNext(t: List<Book>) {
            books = t
            this@BooksActivityViewModel.postBooks(mapper.toRowListAsc(t))
        }

        override fun onError(t: Throwable?) {
             this@BooksActivityViewModel.postError(t)
        }
    }

    fun postError(error: Throwable?) {
        val message = error?.message
                ?: "Oops!! Something happened with the plumbing. Can you try again?"
        liveData.postValue(BookActivityState(message = message, errored =  true))
    }

    fun postBooks(t: List<BookRow>) {
        liveData.postValue(BookActivityState(books = t))
    }
}