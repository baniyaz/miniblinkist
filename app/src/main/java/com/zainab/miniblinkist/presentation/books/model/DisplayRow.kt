package com.zainab.miniblinkist.presentation.books.model

open class DisplayRow(
        override val id: String,
        override val title: String,
        override val author: String,
        override val description: String,
        override val publishedDate: String,
        override val thumbnail: String,
        override val premium: Boolean): BookRow(id, title, author, description, publishedDate, thumbnail, premium) {

    override val type = BookRow.DISPLAY
}