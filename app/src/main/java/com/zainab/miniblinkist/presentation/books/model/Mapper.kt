package com.zainab.miniblinkist.presentation.books.model

import com.zainab.miniblinkist.domain.model.Book
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class Mapper @Inject constructor(){

    private fun toRowList(books: List<Book>): List<BookRow> {
        val calendar = Calendar.getInstance()
        var currentSectionWeek = 0
        var currentSectionYear = 0
        val list = mutableListOf<BookRow>()
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

        books.forEach {
                   calendar.time = sdf.parse(it.publishedDate)

                    val bookWeek = calendar.get(Calendar.WEEK_OF_YEAR)
                    val bookYear = calendar.get(Calendar.YEAR)

                    if(currentSectionWeek != bookWeek || currentSectionYear != bookYear ){
                        currentSectionWeek = bookWeek
                        currentSectionYear = bookYear

                        val section = "Week $bookWeek, $bookYear"
                        list.add(PublishedWeekRow(section))
                    }

                    list.add(DisplayRow(it.id, it.title, it.author, it.description, it.publishedDate, it.thumbnail, it.premium))
                }

        return list
    }

    fun toRowListDesc(books: List<Book>) = toRowList(books.sortedByDescending { book -> book.publishedDate })

    fun toRowListAsc(books: List<Book>) = toRowList(books.sortedBy { book -> book.publishedDate })
}
