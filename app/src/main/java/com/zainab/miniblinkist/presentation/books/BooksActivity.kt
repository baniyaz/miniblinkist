package com.zainab.miniblinkist.presentation.books

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.zainab.miniblinkist.R
import com.zainab.miniblinkist.extensions.hide
import com.zainab.miniblinkist.extensions.show
import com.zainab.miniblinkist.presentation.books.model.BookActivityState
import com.zainab.miniblinkist.presentation.books.model.BookRow
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_books.*
import kotlinx.android.synthetic.main.content_main.*
import javax.inject.Inject

class BooksActivity : AppCompatActivity() {
    @Inject
    lateinit var booksAdapter: BooksAdapter
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(BooksActivityViewModel::class.java) }
    private val observer: Observer<BookActivityState> = createObserver()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_books)

        setSupportActionBar(toolbar)

        prepareViews()

        observeViewModels()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_sort_ascending -> {
                viewModel.sortAscending()
                true }
            R.id.action_sort_descending -> {
                viewModel.sortDescending()
                true}
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun createObserver(): Observer<BookActivityState> {
        return Observer { it ->
            if(it?.errored == true){
                showError(it.message)
            }else renderList(it?.books)
        }
    }

    private fun renderList(it: List<BookRow>?) {
        if (it == null || it.isEmpty()) {
            latest_movies_progressBar.show()
            return
        }

        latest_movies_progressBar.hide()
        booksAdapter.setBooks(it)
    }

    private fun showError(message: String) {
        latest_movies_message.show()
        latest_movies_message.text = message

        latest_movies_progressBar.hide()
    }

    private fun observeViewModels() {
        viewModel.getBooksLiveData().observe(this, observer)
    }

    private fun prepareViews() {
        latest_movies_list.adapter = booksAdapter
        booksAdapter.setOnItemClickListener { showBookInfo(it) }
    }

    private fun showBookInfo(it: BookRow) {

    }

}
