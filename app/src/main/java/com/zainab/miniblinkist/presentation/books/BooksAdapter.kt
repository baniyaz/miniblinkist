package com.zainab.miniblinkist.presentation.books

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zainab.miniblinkist.R
import com.zainab.miniblinkist.extensions.fromUrl
import com.zainab.miniblinkist.presentation.books.model.BookRow
import com.zainab.miniblinkist.presentation.books.model.DisplayRow
import com.zainab.miniblinkist.presentation.books.model.PublishedWeekRow
import kotlinx.android.synthetic.main.book_item.view.*
import kotlinx.android.synthetic.main.published_week_item.view.*
import javax.inject.Inject

class BooksAdapter @Inject constructor() : RecyclerView.Adapter<BooksAdapter.ViewHolder>() {
    private  var books = listOf<BookRow>()
    private  var onItemClickListener: (BookRow) -> Unit ={}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = when (viewType) {
        BookRow.PUBLISHED_WEEK -> ViewHolder.PublishedWeekViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.published_week_item, parent, false))
        BookRow.DISPLAY -> ViewHolder.BookViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.book_item, parent, false))
        else -> throw IllegalStateException("You cannot create a view with invalid type $viewType")
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val bookRow = books[position]

        when(holder) {
            is ViewHolder.BookViewHolder -> {
                holder.bind(bookRow as DisplayRow, onItemClickListener)
            }

            is ViewHolder.PublishedWeekViewHolder -> {
                holder.bind(bookRow as PublishedWeekRow)
            }
        }
    }

    override fun getItemViewType(position: Int) = books[position].type

    override fun getItemCount() = books.size

    fun setBooks(books: List<BookRow>) {
        this.books = books
        this.notifyDataSetChanged()
    }

    fun setOnItemClickListener(onItemClickListener: (BookRow) -> Unit) {
        this.onItemClickListener = onItemClickListener
    }
    
    sealed class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        class BookViewHolder(private val view: View) : ViewHolder(view) {
            fun bind(bookRow: BookRow, onItemClickListener: (BookRow) -> Unit) {
                with(bookRow) {
                    view.book_item_imageView.fromUrl(thumbnail)
                    view.book_item_author.text = author
                    view.book_item_title.text = title
                    view.book_item_overview.text = description
                    view.book_item_publish_date.text = publishedDate
                    view.setOnClickListener { onItemClickListener(this)  }
                }
            }
        }

        class PublishedWeekViewHolder(private val view: View) : ViewHolder(view) {
            fun bind(publishedWeekRow: PublishedWeekRow) {
                    view.published_week.text = publishedWeekRow.publishedWeek
            }
        }
    }
}
