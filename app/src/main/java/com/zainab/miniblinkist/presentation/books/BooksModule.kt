package com.zainab.miniblinkist.presentation.books

import android.arch.lifecycle.ViewModel
import com.zainab.miniblinkist.presentation.dependencyinjection.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class BooksModule {
    @Binds
    @IntoMap
    @ViewModelKey(BooksActivityViewModel::class)
    abstract fun bindBooksActivityViewModel(viewModel: BooksActivityViewModel): ViewModel
}