package com.zainab.miniblinkist.domain

import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.zainab.miniblinkist.data.BooksRepository
import com.zainab.miniblinkist.domain.usecase.FetchRemoteBooksUseCase
import io.reactivex.Completable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class FetchRemoteBooksUseCaseTest {

    @Mock
    lateinit var booksRepository: BooksRepository

     private val schedulersFactory = TestSchedulerFactory()

    private lateinit var fetchRemoteBooksUseCase: FetchRemoteBooksUseCase

    @Before
    fun setUp() {
        fetchRemoteBooksUseCase = FetchRemoteBooksUseCase(booksRepository, schedulersFactory)
    }

    @Test
    fun shouldFetcheRemoteBooksFromRepository(){
        stubBookRepositoryFetchRemoteBooks(Completable.complete())

        fetchRemoteBooksUseCase.build(null)

        verify(booksRepository).fetchRemoteBooks()
    }

    @Test
    fun buildUseCaseCompletes() {
        stubBookRepositoryFetchRemoteBooks(Completable.complete())
        val testObserver = fetchRemoteBooksUseCase.build(null).test()
        testObserver.assertComplete()
    }

    @Test
    fun buildUseCaseReturnError() {
        val throwable = Throwable("ErrorResponse")
        stubBookRepositoryFetchRemoteBooks(Completable.error(throwable))
        val testObserver = fetchRemoteBooksUseCase.build(null).test()
        testObserver.assertError(throwable)
    }

    private fun stubBookRepositoryFetchRemoteBooks(completable: Completable) {
        whenever(booksRepository.fetchRemoteBooks())
                .thenReturn(completable)
    }

}