package com.zainab.miniblinkist.domain

import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import com.nhaarman.mockito_kotlin.whenever
import com.zainab.miniblinkist.data.BooksRepository
import com.zainab.miniblinkist.domain.model.Book
import com.zainab.miniblinkist.domain.usecase.GetLocalBooksUseCase
import com.zainab.miniblinkist.presentation.SchedulersFactory
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class GetLocalBooksUseCaseTest {
    val book = Book("jd7ee", "The Manager's Path", "Camille",
            "For all mangers", "2009-10-23", "", false)

    @Mock
    lateinit var booksRepository: BooksRepository
    @Mock
    lateinit var schedulersFactory: SchedulersFactory

    private lateinit var getLocalBooksUseCase: GetLocalBooksUseCase

    @Before
    fun setUp() {
        getLocalBooksUseCase = GetLocalBooksUseCase(booksRepository, schedulersFactory)
    }

    @Test
    fun shouldGetLocalBooksFromRepository() {
        getLocalBooksUseCase.build(null)

        verify(booksRepository).getLocalBooks()
        verifyNoMoreInteractions(booksRepository)
        verifyZeroInteractions(schedulersFactory)
    }

    @Test
    fun buildUseCaseCompletes() {
        stubBookRepositoryGetLocalBooks(Flowable.just(listOf(book)))
        val testObserver = getLocalBooksUseCase.build(null).test()
        testObserver.assertComplete()
    }

    @Test
    fun buildUseCaseReturnsData() {
        val bookList = listOf(book, book)
        stubBookRepositoryGetLocalBooks(Flowable.just(bookList))
        val testObserver = getLocalBooksUseCase.build(null).test()
        testObserver.assertValue(bookList)
    }

    private fun stubBookRepositoryGetLocalBooks(flowable: Flowable<List<Book>>) {
        whenever(booksRepository.getLocalBooks())
                .thenReturn(flowable)
    }
}