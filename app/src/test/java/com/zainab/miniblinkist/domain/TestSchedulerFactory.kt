package com.zainab.miniblinkist.domain

import io.reactivex.schedulers.Schedulers

class TestSchedulerFactory : ISchedulerFactory {
    override fun main() = Schedulers.trampoline()

    override fun io() = Schedulers.trampoline()

    override fun computation() = Schedulers.trampoline()

    override fun trampoline() = Schedulers.trampoline()
}