package com.zainab.miniblinkist.presentation

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.nhaarman.mockito_kotlin.*
import com.zainab.miniblinkist.domain.model.Book
import com.zainab.miniblinkist.domain.usecase.FetchRemoteBooksUseCase
import com.zainab.miniblinkist.domain.usecase.GetLocalBooksUseCase
import com.zainab.miniblinkist.domain.usecase.base.UsecaseObserver
import com.zainab.miniblinkist.presentation.books.BooksActivityViewModel
import com.zainab.miniblinkist.presentation.books.model.BookActivityState
import com.zainab.miniblinkist.presentation.books.model.DisplayRow
import com.zainab.miniblinkist.presentation.books.model.Mapper
import com.zainab.miniblinkist.presentation.books.model.PublishedWeekRow
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class BooksActivityViewModelTest {
    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var getLocalBooksUseCase: GetLocalBooksUseCase
    @Mock
    lateinit var fetchRemoteBooksUseCase: FetchRemoteBooksUseCase
    @Mock
    lateinit var mapper: Mapper

    private val observerState = mock<Observer<BookActivityState>>()
    var useCaseFlowableObserverArgumentCaptor: KArgumentCaptor<UsecaseObserver.RxFlowable<List<Book>>> = argumentCaptor()

    private val bookRowListAsc = listOf(PublishedWeekRow("Week 1, 2009"),
            DisplayRow("jd7ee", "The Manager's Path", "Camille",
                    "For all mangers", "2009-01-01", "",
                    false),
            PublishedWeekRow("Week 1, 2010"),
            DisplayRow("jd7ee", "The Manager's Path", "Camille",
                    "For all mangers", "2010-01-01", "", false))

    private val bookRowListDesc = listOf(PublishedWeekRow("Week 1, 2010"), DisplayRow("jd7ee", "The Manager's Path", "Camille",
            "For all mangers", "2010-01-01", "", false), PublishedWeekRow("Week 1, 2009"), DisplayRow("jd7ee", "The Manager's Path", "Camille",
            "For all mangers", "2009-01-01", "", false))

    private val expectedResultsState = BookActivityState(books = bookRowListAsc)
    private val expectedResultsDescState = BookActivityState(books = bookRowListDesc)
    private val expectedErrorState = BookActivityState(message = "Error Response", errored = true)
    private lateinit var bookActivityStateCaptor: KArgumentCaptor<BookActivityState>

    private lateinit var viewModel: BooksActivityViewModel

    @Before
    fun setup() {
        viewModel = BooksActivityViewModel(getLocalBooksUseCase, fetchRemoteBooksUseCase, mapper)
        reset( observerState)
    }

    @Test
    fun verifyDataSyncsFromRemoteOnCreate() {
        verify(fetchRemoteBooksUseCase).execute(any(), any(), eq(null))
    }

    @Test
    fun verifyViewModelBindsToStream() {
        given(mapper.toRowListAsc(any())).willReturn(bookRowListAsc)

        viewModel.getBooksLiveData().observeForever(observerState)

        useCaseFlowableObserverArgumentCaptor.run {
            verify(getLocalBooksUseCase).execute(capture(), eq(null))
            firstValue.onNext(emptyList())
        }

        bookActivityStateCaptor = argumentCaptor()
        bookActivityStateCaptor.run {
            verify(observerState, times(1)).onChanged(capture())
            assertThat(firstValue).isEqualToComparingFieldByField(expectedResultsState)
        }

        verify(mapper).toRowListAsc(any())
    }

    @Test
    fun verifyBooksErrorState() {
        viewModel.getBooksLiveData().observeForever(observerState)

        useCaseFlowableObserverArgumentCaptor.run {
            verify(getLocalBooksUseCase).execute(capture(), eq(null))
            firstValue.onError(Throwable("Error Response"))
        }

        bookActivityStateCaptor = argumentCaptor()
        bookActivityStateCaptor.run {
            verify(observerState, times(1)).onChanged(capture())
            assertThat(firstValue).isEqualToComparingFieldByField(expectedErrorState)
        }
    }

    @Test
    fun verifyViewModelSortsBookDescending() {
        given(mapper.toRowListDesc(any())).willReturn(bookRowListDesc)

        viewModel.getBooksLiveData().observeForever(observerState)

        viewModel.sortDescending()

        bookActivityStateCaptor = argumentCaptor()
        bookActivityStateCaptor.run {
            verify(observerState, times(1)).onChanged(capture())
            assertThat(firstValue).isEqualToComparingFieldByField(expectedResultsDescState)
        }

        verify(mapper).toRowListDesc(any())
        verifyNoMoreInteractions(mapper)
    }

}