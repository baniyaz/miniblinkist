package com.zainab.miniblinkist.data.local

import com.nhaarman.mockito_kotlin.inOrder
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.zainab.miniblinkist.data.local.BookEntity
import com.zainab.miniblinkist.data.local.BooksDao
import com.zainab.miniblinkist.data.local.BooksReactiveStore
import io.reactivex.Flowable
import io.reactivex.subscribers.TestSubscriber
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class BooksReactiveStoreTest {
    @Mock lateinit var booksDao: BooksDao

    private lateinit var reactiveStore: BooksReactiveStore

    @Before
    fun setup() {
        reactiveStore = BooksReactiveStore(booksDao)
    }

    @Test
    fun storeAll_shouldCallDao() {
          val list = listOf(createBookEntity())

        reactiveStore.storeAll(list)

        verify(booksDao).insertAll(list)
    }

    @Test
    fun testReplaceAll() {
         val list = listOf(createBookEntity())

        reactiveStore.replaceAll(list)

        val inoder = inOrder(booksDao)
        inoder.verify(booksDao).deleteAll()
        inoder.verify(booksDao).insertAll(list)
    }

    @Test
    fun getAll_shouldReturnEmpty_whenNoRecordInDb() {
        whenever(booksDao.getBooks()).thenReturn(Flowable.just(Collections.emptyList()))

          val subscriber: TestSubscriber<List<BookEntity>> = reactiveStore.getAll().test()

        subscriber.assertNoErrors()
        subscriber.assertResult(Collections.emptyList())
    }

    @Test
    fun getAll_shouldReturnRecordsInDb_whenNotEmpty() {
        val list = listOf(createBookEntity())
        whenever(booksDao.getBooks()).thenReturn(Flowable.just(list))

        val subscriber: TestSubscriber<List<BookEntity>> = reactiveStore.getAll().test()

        subscriber.assertNoErrors()
        subscriber.assertResult(list)
    }

    private fun createBookEntity(): BookEntity {
        return BookEntity("jd7ee", "The Manager's Path", "Camille", "For all mangers", "2009-10-23",
                "", false)
    }
}