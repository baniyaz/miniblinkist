package com.zainab.miniblinkist.data

import com.zainab.miniblinkist.data.local.BookEntity
import com.zainab.miniblinkist.domain.model.Book
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class MapperTest {

    val book = Book("jd7ee", "The Manager's Path", "Camille",
            "For all mangers", "2009-10-23", "", false)

    val bookEntity = BookEntity("jd7ee", "The Manager's Path", "Camille",
            "For all mangers", "2009-10-23", "", false)

    private lateinit var mapper:Mapper

    @Before
    fun setUp() {
        mapper = Mapper()
    }

    @Test
    fun shouldConvertBookToBookEntity(){
        val result = mapper.toBookEntity(book)

        assertThat(result).isEqualToComparingFieldByField(bookEntity)
    }

    @Test
    fun shouldConvertBookEntityToBookList(){
        val result = mapper.toBookList(listOf(bookEntity))
        assertThat(result).isEqualTo(listOf(book))
    }
}