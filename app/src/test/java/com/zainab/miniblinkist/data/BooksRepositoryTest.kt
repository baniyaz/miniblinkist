package com.zainab.miniblinkist.data

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.zainab.miniblinkist.data.local.BookEntity
import com.zainab.miniblinkist.data.remote.BookService
import com.zainab.miniblinkist.domain.model.Book
import com.zainab.miniblinkist.presentation.SchedulersFactory
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class BooksRepositoryTest {
    @Mock lateinit var bookService: BookService
    @Mock lateinit var schedulersFactory: SchedulersFactory
    @Mock lateinit var reactiveStore: ReactiveStore<BookEntity>

    private var mapper: Mapper = Mapper()
    private lateinit var booksRepository: BooksRepository
    private val testScheduler = TestScheduler()
    private val ioScheduler = TestScheduler()

    @Before
    fun setup() {
        booksRepository = BooksRepository(bookService, schedulersFactory, reactiveStore, mapper)
        whenever(schedulersFactory.computation()).thenReturn(testScheduler)
        whenever(schedulersFactory.io()).thenReturn(ioScheduler)
    }

    @Test
    fun testGetBooksListGetsFromStore() {
         val list = listOf(createBookEntity())
        whenever(reactiveStore.getAll()).thenReturn(Flowable.just(list))

        booksRepository.getLocalBooks()

        verify(reactiveStore).getAll()
    }

    @Test
    fun testFetchBooks() {
        whenever(bookService.getBooks()).thenReturn(Single.just(createApiResponse()))

        val observer: TestObserver<Void> = booksRepository.fetchRemoteBooks().test()

        ioScheduler.triggerActions()
        testScheduler.triggerActions()

        observer.awaitTerminalEvent()
        observer.assertComplete()
        observer.assertNoErrors()

        verify(reactiveStore).replaceAll(any())
    }

    private fun createApiResponse(): List<Book> {
        val book = Book("jd7ee", "The Manager's Path", "Camille",
                "For all mangers", "2009-10-23", "", false)
        return  listOf(book)
    }

    private fun createBookEntity(): BookEntity {
        return BookEntity("jd7ee", "The Manager's Path", "Camille",
                "For all mangers", "2009-10-23", "", false)
    }
}