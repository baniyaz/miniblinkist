# README #

This is app is a mini book library. It was built using the MVVM pattern and 100% in Kotlin. 

It pulls a list of books from a mock api and displays it in sections according to the week it was published.

### How do I get set up? ###

* Android Studio 3.0
* Android Gradle Plugin 
* Kotlin 1.2.0

###Libraries Used  ###

* Design SupportLib
* Retrofit
* Gson
* Ok Http
* Picasso
* Architechture Components - (Room and ArchLifeCycle)
* Dagger (Dagger Android)
* RxJava/RxAndroid
* JUnit
* Mockito (Mockito-Kotlin)